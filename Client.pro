#-------------------------------------------------
#
# Project created by QtCreator 2016-09-10T11:09:07
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app


SOURCES += sources/main.cpp \
    sources/Client.cpp \
    sources/Settings.cpp

HEADERS  += sources/Client.h \
    sources/Settings.h \
    sources/constants.h

FORMS    += sources/Client.ui \
    sources/Settings.ui
