#include "Settings.h"
#include "ui_Settings.h"

Settings::Settings(const QString &address, const uint16_t port, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Settings)
{
  ui->setupUi(this);
  ui->lineEditIp->setText(address);
  ui->lineEditPort->setText(QString::number(port));

  QString octet = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
  QRegExp ip = QRegExp(octet + "\\." + octet + "\\." + octet + "\\." + octet);
  QRegExpValidator *validator = new QRegExpValidator(ip);
  ui->lineEditIp->setValidator(validator);

  connect(ui->buttonBox, SIGNAL(accepted()), SLOT(slotOk()));
}

Settings::~Settings()
{
  delete ui;
}

const QString& Settings::getAddress() const
{
  return m_address;
}

const uint16_t Settings::getPort() const
{
  return m_port;
}

void Settings::slotOk()
{
  m_address = ui->lineEditIp->text();
  m_port = ui->lineEditPort->text().toShort();
  close();
}
