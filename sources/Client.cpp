#include "Client.h"
#include "ui_Client.h"

Client::Client(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::Client)
{
  ui->setupUi(this);

  m_tcpSocket = nullptr;
  m_hostAddress = "127.0.0.1";
  m_port = 2323;
  m_nextBlockSize = 0;

  connect(ui->actionConnect, SIGNAL(triggered(bool)), SLOT(slotConnectToHost()));
  connect(ui->actionDisconnect, SIGNAL(triggered(bool)), SLOT(slotDisconnectFromHost()));
  connect(ui->actionSettings, SIGNAL(triggered(bool)), SLOT(slotSettings()));
  connect(ui->actionQuit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

  ui->lineEdit->setEnabled(false);
  ui->pushButton->setEnabled(false);
  ui->actionDisconnect->setEnabled(false);
  ui->actionConnect->setEnabled(true);
}

Client::~Client()
{
  if (m_tcpSocket)
  {
    m_tcpSocket->disconnectFromHost();
    delete m_tcpSocket;
  }
  delete ui;
}

void Client::slotConnectToHost()
{
  m_tcpSocket = new QTcpSocket(this);
  m_tcpSocket->connectToHost(m_hostAddress, m_port);

  connect(m_tcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
  connect(m_tcpSocket, SIGNAL(disconnected()), SLOT(slotDisconnected()));
  connect(m_tcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
  connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
          this, SLOT(slotError(QAbstractSocket::SocketError))
          );
  connect(ui->pushButton, SIGNAL(clicked(bool)), SLOT(slotSendToServer()));
  connect(ui->lineEdit, SIGNAL(returnPressed()), SLOT(slotSendToServer()));
}

void Client::slotDisconnectFromHost()
{
  m_tcpSocket->disconnectFromHost();
  ui->actionConnect->setEnabled(true);
  ui->actionDisconnect->setEnabled(false);
  delete m_tcpSocket;
  m_tcpSocket = nullptr;
}

void Client::slotReadyRead()
{
  QDataStream in(m_tcpSocket);
  in.setVersion(QDataStream::Qt_5_7);
  for(;;)
  {
    if(!m_nextBlockSize)
    {
      if(m_tcpSocket->bytesAvailable() < SIZE)
        break;
      in >> m_nextBlockSize;
    }

    if(m_tcpSocket->bytesAvailable() < m_nextBlockSize)
    {
      m_nextBlockSize = 0;
      break;
    }

    QTime time;
    QString str;
    in >> time >> str;

    ui->textEdit->append(time.toString() + " " + str);
    m_nextBlockSize = 0;
  }
}

void Client::slotError(QAbstractSocket::SocketError err)
{
  QString strError =
      err == QAbstractSocket::HostNotFoundError ?
        "Сервер не найден." :
        err == QAbstractSocket::RemoteHostClosedError ?
          "Удаленный узел закрыт." :
          err == QAbstractSocket::ConnectionRefusedError ?
            "В соединении отказано." :
            QString(m_tcpSocket->errorString());
  ui->textEdit->append(strError);

  m_tcpSocket->disconnectFromHost();
}

void Client::slotSendToServer()
{
  if (ui->lineEdit->text() != "")
  {
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_7);
    out << static_cast<uint16_t>(0) << QTime::currentTime() << ui->lineEdit->text();

    out.device()->seek(0);
    out << static_cast<uint16_t>(block.size() - SIZE);

    m_tcpSocket->write(block);
    ui->lineEdit->clear();
  }
}

void Client::slotConnected()
{
  ui->textEdit->append("Соединение установлено.");

  ui->lineEdit->setEnabled(true);
  ui->pushButton->setEnabled(true);
  ui->actionConnect->setEnabled(false);
  ui->actionDisconnect->setEnabled(true);
}

void Client::slotDisconnected()
{
//  ui->textEdit->append("Соединение разорвано.");

  disconnect(m_tcpSocket, SIGNAL(connected()),
             this, SLOT(slotConnected())
             );
  disconnect(m_tcpSocket, SIGNAL(disconnected()),
             this, SLOT(slotDisconnected())
             );
  disconnect(m_tcpSocket, SIGNAL(readyRead()),
             this, SLOT(slotReadyRead())
             );
  disconnect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
             this, SLOT(slotError(QAbstractSocket::SocketError))
             );

  ui->lineEdit->setEnabled(false);
  ui->pushButton->setEnabled(false);
  ui->actionDisconnect->setEnabled(false);
  ui->actionConnect->setEnabled(true);
}

void Client::slotSettings()
{
  Settings *settingsWindow = new Settings(m_hostAddress, m_port, this);

  if(settingsWindow->exec() == QDialog::Accepted)
  {
    m_hostAddress = settingsWindow->getAddress();
    m_port = settingsWindow->getPort();

    ui->statusBar->showMessage("Настройки будут применены после переподключения",
                               3000);
  }
}
