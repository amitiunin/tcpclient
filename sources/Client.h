#ifndef CLIENT_H
#define CLIENT_H

#include "constants.h"
#include "Settings.h"

#include <QTime>
#include <QTcpSocket>
#include <QDataStream>
#include <QMainWindow>

namespace Ui {
class Client;
}

class Client : public QMainWindow
{
  Q_OBJECT

public:
  explicit Client(QWidget *parent = 0);
  ~Client();

private:
  Ui::Client *ui;
  QString m_hostAddress;
  uint16_t m_port;
  QTcpSocket *m_tcpSocket;
  uint16_t m_nextBlockSize;

private slots:
  void slotConnectToHost();
  void slotDisconnectFromHost();
  void slotReadyRead();
  void slotError(QAbstractSocket::SocketError err);
  void slotSendToServer();
  void slotConnected();
  void slotDisconnected();
  void slotSettings();
};

#endif // CLIENT_H
