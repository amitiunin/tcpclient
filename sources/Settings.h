#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
  Q_OBJECT

public:
  explicit Settings(const QString &address, const uint16_t port, QWidget *parent = 0);
  ~Settings();

  const QString& getAddress() const;
  const uint16_t getPort() const;

private:
  Ui::Settings *ui;
  QString m_address;
  uint16_t m_port;

private slots:
  void slotOk();
};

#endif // SETTINGS_H
